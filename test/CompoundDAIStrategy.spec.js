require('dotenv').config();
const daiAbi = require('../scripts/abis/dai-abi.json');
const cDaiAbi = require('../scripts/abis/cdai-abi.json');
const compAbi = require('../scripts/abis/comp-abi.json');
const CompoundDAIStrategy = artifacts.require('CompoundDAIStrategy');
const { BN, ether, time, expectRevert } = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const daiMainNetAddress = process.env.DAI_MAINNET_ADDR;
const daiMcdJoin = process.env.DAI_MCD_JOIN;
const soloMargin = process.env.SOLO_MARGIN_ADDR;
const cDaiAddress = process.env.CDAI_ADDR;
const compTokenAddress = process.env.COMP_ADRR;

contract('CompoundDAIStrategy', async (accounts) => {
    let daiContract;
    let strategyContract;
    let cDaiContract;
    let strategyContractAddr;
    let compContract;
    const owner = accounts[0];

    before(async function() {
        daiContract = new web3.eth.Contract(daiAbi, daiMainNetAddress);
        strategyContract = await CompoundDAIStrategy.deployed();
        strategyContractAddr = strategyContract.address;
        cDaiContract = new web3.eth.Contract(cDaiAbi, cDaiAddress);
        compContract = new web3.eth.Contract(compAbi, compTokenAddress);
    });

    afterEach(async function() {
        const balance = await daiContract.methods.balanceOf(strategyContractAddr).call();

        if (balance > 0) {
            // Return DAI to sender
            await daiContract
                .methods
                .transferFrom(strategyContractAddr, accounts[0], balance)
                .send({ from: accounts[0] });
        }
    });

    const mintDai = async (numbDaiToSupply) => {
        await daiContract.methods.mint(accounts[0], numbDaiToSupply)
            .send({
                from: daiMcdJoin,
                gasPrice: web3.utils.toHex(0)
            });
    }

    const getBalances = async () => {
        const result = await cDaiContract.methods.getAccountSnapshot(strategyContractAddr).call();

        const ctokenBalance = new BN(result[1]);
        const borrowBalance = new BN(result[2]);
        const exRate = new BN(result[3]);
        const underlyingBalance = exRate.mul(ctokenBalance) / 1e18;

        return [ctokenBalance, borrowBalance, underlyingBalance];
    }

    it('supplies/redeems DAI to/from Compound', async () => {
        const expectedDAI = ether('20');
        await daiContract.methods.approve(strategyContractAddr, expectedDAI).send({ from: accounts[0] });

        await strategyContract.supplyDai(expectedDAI, 0);

        const ctokenBalanceBefore = await cDaiContract.methods.balanceOf(strategyContractAddr).call();
        await time.advanceBlock();
        const ctokenBalanceAfter = await cDaiContract.methods.balanceOf(strategyContractAddr).call();

        expect(ctokenBalanceAfter).to.be.bignumber.at.least(ctokenBalanceBefore);

        await strategyContract.redeemAll();
        const balanceAfter = await daiContract.methods.balanceOf(owner).call();
        expect(balanceAfter)
            .to.be.bignumber.at.least(expectedDAI);
    });

    it('inflates DAIs', async () => {
        const expectedDAI = ether('20');
        await daiContract.methods.approve(strategyContractAddr, expectedDAI.addn(2)).send({ from: accounts[0] });

        await strategyContract.supplyDai(expectedDAI, 2);
        await strategyContract.inflate(expectedDAI);

        const [, borrowBalance,] = await getBalances();
        expect(borrowBalance).to.be.bignumber.equal(expectedDAI);
    });

    it('deflates DAIs', async () => {
        await daiContract.methods.approve(strategyContractAddr, 2).send({ from: accounts[0] });
        await strategyContract.supplyDai(0, 2);

        const ctokenBalanceBefore = await cDaiContract.methods.balanceOf(strategyContractAddr).call();

        await strategyContract.deflate();

        const ctokenBalanceAfter = await cDaiContract.methods.balanceOf(strategyContractAddr).call();

        expect(ctokenBalanceAfter)
            .to.be.bignumber.below(ctokenBalanceBefore);

        const [, borrowBalance,] = await getBalances();
        expect(borrowBalance).to.be.bignumber.equal(new BN(0));
    });

    it('should claim COMP tokens', async () => {
        await strategyContract.claimComp();
        const compBalance = await compContract.methods.balanceOf(owner).call();

        expect(compBalance)
            .to.be.bignumber.above(new BN('0'));

        await strategyContract.redeemAll();
    });

    it('fails when inflated above collateral factor', async () => {
        const expectedDAI = ether('10');

        await daiContract.methods.approve(strategyContractAddr, expectedDAI.addn(2)).send({ from: accounts[0] });
        await strategyContract.supplyDai(expectedDAI, 2);

        await expectRevert(
            strategyContract.inflate(
                expectedDAI.muln(3).add(ether('1'))
            ),
            "CompoundDAIStrategy: Insuficient collateral."
        );

        await strategyContract.redeemAll();
    });

    it('fails when inflated above DyDx pool', async () => {
        const flashLoanSize = new BN(
            await daiContract.methods.balanceOf(soloMargin).call()
        );
        const numbDaiToSupply = flashLoanSize.shrn(1); // div(2)

        await mintDai(numbDaiToSupply.addn(4));

        await daiContract.methods.approve(strategyContractAddr, numbDaiToSupply.addn(4)).send({ from: accounts[0] });
        await strategyContract.supplyDai(numbDaiToSupply, 4);

        try {
            await strategyContract.inflate(flashLoanSize.addn(1));
            expect.fail("Invalid step.");
        } catch(err) {
            const reason = err.reason;
            expect(reason).to.equal("CompoundDAIStrategy: Not enough DAIs in DyDx pool.");
        }

        await strategyContract.redeemAll();
    });

    it('fails when in(de)flated with not enough DAIs on balance', async () => {
        try {
            await strategyContract.inflate(ether('10'));
            expect.fail("Invalid step.");
        } catch(err) {
            const reason = err.reason;
            expect(reason).to.equal("CompoundDAIStrategy: Not enough DAIs for DyDx flashloan.");
        }
    });

    it('fails when not owner tries to call external function', async () => {
        await expectRevert(
            strategyContract.inflate(ether('1'), { from: accounts[1] }),
            "CompoundDAIStrategy: not owner."
        );

        await expectRevert(
            strategyContract.deflate({ from: accounts[1] }),
            "CompoundDAIStrategy: not owner."
        );
        await expectRevert(
            strategyContract.supplyDai(ether('1'), 2, { from: accounts[1] }),
            "CompoundDAIStrategy: not owner."
        );

        await expectRevert(
            strategyContract.redeemAll({ from: accounts[1] }),
            "CompoundDAIStrategy: not owner."
        );

        await expectRevert(
            strategyContract.claimComp({ from: accounts[1] }),
            "CompoundDAIStrategy: not owner."
        );
    });
});