const daiAbi = require('./abis/dai-abi.json');
const CDai = artifacts.require('ICERC20');
const COMP = artifacts.require('IERC20');
const CompoundDAIStrategy = artifacts.require('CompoundDAIStrategy');
const { BN, ether } = require('@openzeppelin/test-helpers');
const { web3 } = require('@openzeppelin/test-helpers/src/setup');
var fs = require('fs');

const daiMainNetAddress = "0x6b175474e89094c44da98b954eedeac495271d0f";
const cDaiAddress = "0x5d3a536E4D6DbD6114cc1Ead35777bAB948E3643";
const COMPAddress = "0xc00e94Cb662C3520282E6f5717214004A7f26888";


async function main() {
    const accounts = await web3.eth.getAccounts();

    const daiContract = new web3.eth.Contract(daiAbi, daiMainNetAddress);
    const balance = await daiContract.methods.balanceOf(accounts[0]).call();

    
    const strategyContract = await CompoundDAIStrategy.deployed();
    const strategyContractAddr = strategyContract.address;

    const cDaiContract = await CDai.at(cDaiAddress);
    const compContracts = await COMP.at(COMPAddress);

    const printInfo = async () => {
        const balanceContract = await daiContract.methods.balanceOf(strategyContractAddr).call();
        console.log(`DAI balance of contract: ${balanceContract / 1e18}`);

        const result = await cDaiContract.getAccountSnapshot(strategyContractAddr);

        const ctokenBalance = result[1];
        const borrowBalance = result[2];
        const exRate = result[3];
        const underlyingBalance = exRate.mul(ctokenBalance) / 1e36;

        console.log(`cDAI balance of contract: ${ctokenBalance / 1e8}`);
        console.log(`Borrow DAI balance: ${borrowBalance / 1e18}`);
        console.log(`Underlying DAI balance: ${underlyingBalance}`);
    }

    // DEFLATE
    {
        console.log("deflate()");
        await strategyContract.deflate();
        await printInfo();
        console.log('_________________________\n\n');
    }

    // REDEEM
    {
        console.log("redeemAll()");
        await strategyContract.redeemAll();
        await printInfo();
        console.log('_________________________\n\n');
    }

    // CLAIM COMP
    {
        console.log("claimComp()");
        await strategyContract.claimComp();
        const balanceContract = await strategyContract.getCompBalance();

        console.log(`COMP balance of contract: ${balanceContract / 1e18}`);
    }
}

module.exports = function(callback) {
    main()
        .then(() => {
            console.log("Ok!");
            callback();
        })
        .catch((err) => {
            console.log(err);
            callback();
        });
}