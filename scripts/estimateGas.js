const daiAbi = require('./abis/dai-abi.json');
const CompoundDAIStrategy = artifacts.require('CompoundDAIStrategy');
const { BN, ether } = require('@openzeppelin/test-helpers');
const { web3 } = require('@openzeppelin/test-helpers/src/setup');

const daiMainNetAddress = "0x6b175474e89094c44da98b954eedeac495271d0f";

async function main() {
    const accounts = await web3.eth.getAccounts();

    const daiContract = new web3.eth.Contract(daiAbi, daiMainNetAddress);
    const balance = await daiContract.methods.balanceOf(accounts[0]).call();

    
    const strategyContract = await CompoundDAIStrategy.deployed();
    const strategyContractAddr = strategyContract.address;

    await daiContract.methods.transfer(strategyContractAddr, ether('20')).send({ from: accounts[0] });

    const balanceContract = await daiContract.methods.balanceOf(strategyContractAddr).call();
    

    await strategyContract.supplyDaiToCompound(ether('10'));


    await strategyContract.inflate(ether('10'));

    const result = await strategyContract.deflate.estimateGas();
    console.log(result);
}

module.exports = function(callback) {
    main()
        .then(() => {
            console.log("Ok!");
            callback();
        })
        .catch((err) => {
            console.log(err);
            callback();
        });
}