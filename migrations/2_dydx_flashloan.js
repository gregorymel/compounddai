const CompoundDAIStrategy = artifacts.require("CompoundDAIStrategy");

const aragonAgentAddr = process.env.ARAGON_AGENT_ADDR;

module.exports = function(deployer) {
  const network = deployer.network;

  if (network == 'development') {
    const owner = deployer.networks.development.from;
    console.log("🚀 ~ file: 2_dydx_flashloan.js ~ line 8 ~ owner", owner);

    deployer.deploy(CompoundDAIStrategy, owner);
  }
  
  if (network == 'rinkeby') {
    const owner = aragonAgentAddr;
    console.log("🚀 ~ file: 2_dydx_flashloan.js ~ line 8 ~ owner", owner);

    deployer.deploy(CompoundDAIStrategy, owner);
  }
};
