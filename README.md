# Compound DAI strategy

This repo contains contracts that implements strategy to increase income in COMP tokens, using Compound protocol and DyDx flashloans.

First run install dependecies:

`npm install -g ganache-cli`

`npm install`


#### Tests

After your forked mainnet with `ganache.sh` is up and running, in a separate terminal run:

`node scripts/dai.js` to mint DAI tokens 

`npx truffle test` to run tests
