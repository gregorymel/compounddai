require('dotenv').config()
const HDWalletProvider = require('@truffle/hdwallet-provider');

const infuraKey = process.env.INFURA_KEY;

// const fs = require('fs');
const privateKeyKovan = process.env.PRIVATE_KEY_KOVAN;
const privateKeyRinkeby = process.env.PRIVATE_KEY_RINKEBY;
const privateKeyMainnet = process.env.PRIVATE_KEY_MAINNET;

module.exports = {
 
  networks: {

    development: {
     host: "127.0.0.1",     // Localhost (default: none)
     port: 8545,            // Standard Ethereum port (default: none)
     network_id: "*",        // Any network (default: none)
     gasPrice: 137000000000
    },

    kovan: {
      provider: () => new HDWalletProvider(privateKeyKovan, `https://kovan.infura.io/v3/${infuraKey}`),
      network_id: 42,       // rinkeby's id
      gas: 6900000,        // Ropsten has a lower block limit than mainnet
      gasPrice: 2000000000,
      skipDryRun: true     // Skip dry run before migrations? (default: false for public nets )
    },

    rinkeby: {
      provider: () => new HDWalletProvider(privateKeyRinkeby, `https://rinkeby.infura.io/v3/${infuraKey}`),
      network_id: 4,       // rinkeby's id
      gas: 6900000,        // Ropsten has a lower block limit than mainnet
      gasPrice: 2000000000,
      skipDryRun: true     // Skip dry run before migrations? (default: false for public nets )
    },

    mainnet: {
      provider: () => new HDWalletProvider(privateKeyMainnet, `https://mainnet.infura.io/v3/${infuraKey}`),
      network_id: 1,
      gasPrice: 137000000000,
    }
  },

  // Set default mocha options here, use special reporters etc.
  mocha: {
    // timeout: 100000
  },

  // Configure your compilers
  compilers: {
    solc: {
      version: "0.7.6",    // Fetch exact version from solc-bin (default: truffle's version)
      // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
      settings: {          // See the solidity docs for advice about optimization and evmVersion
       optimizer: {
         enabled: true,
         runs: 200
       },
      //  evmVersion: "byzantium"
      }
    }
  }
}
