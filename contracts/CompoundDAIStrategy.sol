// SPDX-License-Identifier: AGPL-3.0-or-later

pragma solidity ^0.7.0;
pragma experimental ABIEncoderV2;

import "./FlashLoanTemplate.sol";

interface ICERC20 is IERC20 {
    function mint(uint256) external returns (uint256);
    function borrow(uint256) external returns (uint);
    function repayBorrow(uint256) external returns (uint);
    function redeem(uint) external returns (uint);
    function redeemUnderlying(uint) external returns (uint);
    function borrowBalanceCurrent(address) external returns (uint);
    function balanceOfUnderlying(address owner) external returns (uint);
}

interface Comptroller {
    function enterMarkets(address[] calldata)
        external
        returns (uint256[] memory);

    function claimComp(address holder) external;
}

library SafeMath {
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }
}

/**
 * @dev CompoundDAIStrategy contract.
 * This contracts implements strategy to incresea income in COMP.
 * @author Grigorii Melnikov <grigorii.melnikov@startblock.online>
 */
contract CompoundDAIStrategy is FlashLoanTemplate {
    using SafeMath for uint256;

    // The cDAI token contract
    ICERC20 private cDAI = ICERC20(0x5d3a536E4D6DbD6114cc1Ead35777bAB948E3643);
    // The COMP token contract
    IERC20 private COMP = IERC20(0xc00e94Cb662C3520282E6f5717214004A7f26888);
    // The Comptroller token contract
    Comptroller private comptroller = Comptroller(0x3d9819210A31b4961b30EF54bE2aeD79B9c9Cd3B);
    
    address public owner;

    // // RINKEBY testnet
    // ICERC20 private cDAI = ICERC20(0x6D7F0754FFeb405d23C51CE938289d4835bE3b14);
    // IERC20 private COMP = IERC20(0x61460874a7196d6a22D1eE4922473664b3E95270);
    // Comptroller private comptroller = Comptroller(0x2EAa9D77AE4D8f9cdD9FAAcd44016E746485bddb);

    // Current collateral factor (k) for DAI = 75%
    // x - initial supply in DAI, y - additional supply in DAI and borrow amount
    // y < k * (x + y) => y < [ k / (1 - k) ] * x
    // COLLATERAL_VALUE = k / (1 - k) = 3
    // uint256 public constant COLLATERAL_VALUE = 3;

    constructor(address agent) FlashLoanTemplate() {
        owner = agent;
        _enterDaiMarket();
        // Approve transfers on the cDAI contract
        DAI.approve(address(cDAI), uint256(-1));
        DAI.approve(owner, uint256(-1));
    }

    modifier onlyOwner() {
        require (owner == msg.sender, "CompoundDAIStrategy: not owner.");
        _;
    }

    function _supplyDaiInternal(uint256 numTokensToSupply) private {
        // Mint cTokens
        uint256 mintResult = cDAI.mint(numTokensToSupply);
        require(mintResult == 0, "CompoundDAIStrategy: mint failed.");
    }

    /**
     * @notice Sender supply DAI tokens to Compound protocol
     * @param numTokensToSupply The amount of DAIs to supply in Compound
     * @param loanFee The amount of loans' fee
     */
    function supplyDai(uint256 numTokensToSupply, uint256 loanFee) external onlyOwner {
        DAI.transferFrom(owner, address(this), numTokensToSupply.add(loanFee));

        _supplyDaiInternal(numTokensToSupply);
    }

    function _redeemUnderlyingInternal(uint256 numTokensToRedeem) private {
        uint256 result = cDAI.redeemUnderlying(numTokensToRedeem);

        require(result == 0, "CompoundDAIStrategy: redeemUnderlying failed.");
    }

    /**
     * @notice Redeem all DAIs from Compound and transfer them to owner
     */
    function redeemAll() external onlyOwner {
        uint256 result = cDAI.redeem(cDAI.balanceOf(address(this)));

        require(result == 0, "CompoundDAIStrategy: redeemAll failed.");
        DAI.transfer(owner, DAI.balanceOf(address(this)));
    }

    function _enterDaiMarket() private {
        address[] memory cTokens = new address[](1);
        cTokens[0] = address(cDAI);
        uint256[] memory errors = comptroller.enterMarkets(cTokens);

        require(errors[0] == 0, "CompoundDAIStrategy: enterMarkets failed.");
    }

    function _borrowDaiFromCompound(uint256 numTokensToBorrow) private {
        uint256 borrowResult = cDAI.borrow(numTokensToBorrow);
        if (borrowResult == 0) {
            return;
        } else if (borrowResult == 3 /* COMPTROLLER_REJECTION */) {
            revert("CompoundDAIStrategy: Insuficient collateral.");
        } else {
            revert("CompoundDAIStrategy: borrow failed.");
        }
    }

    function _repayBorrow(uint256 repayAmmount) private {
        uint256 error = cDAI.repayBorrow(repayAmmount);

        require(error == 0, "CompoundDAIStrategy: repayment borrow failed.");
    }

    /**
     * @notice Sender takes a flashloan, supplies loan on Compound and borrows the same amount to return loan
     * @dev Size of the flashloan should be less than COLLATERAL FACTOR * balanceUnderlying
     * @param numTokensToInflate The amount of DAI tokens to inflate
     */
    function inflate(uint256 numTokensToInflate) external onlyOwner {
        require(numTokensToInflate > 0, "CompoundDAIStrategy: Inflate request with zero amount.");
        require(DAI.balanceOf(address(this)) >= 2, "CompoundDAIStrategy: Not enough DAIs for DyDx flashloan.");

        require(
            numTokensToInflate <= DAI.balanceOf(address(soloMargin)),
            "CompoundDAIStrategy: Not enough DAIs in DyDx pool."
        );
        _flashLoan(numTokensToInflate, OperationType.Inflate);
    }

    /**
     * @notice Sender takes a flashloan, repays borrow and redeem underlying tokens to return loan
     */
    function deflate() external onlyOwner {
        require(DAI.balanceOf(address(this)) >= 2, "CompoundDAIStrategy: Not enoug DAIs for DyDx flashloan.");

        uint256 borrowBalance = cDAI.borrowBalanceCurrent(address(this));

        require(
            borrowBalance <= DAI.balanceOf(address(soloMargin)),
            "CompoundDAIStrategy: Not enough DAIs in DyDx pool."
        );
        _flashLoan(borrowBalance, OperationType.Deflate);
    }

    function callFunction(address /* sender */, Account.Info memory /* accountInfo */, bytes memory data)
        external 
        override 
    {
        require(msg.sender == address(soloMargin), "CompoundDAIStrategy: Only DyDx Solo margin contract can call.");

        // This must match the variables defined in the FlashLoanTemplate
        (
            uint loanAmount,
            OperationType opType
        ) = abi.decode(data, (
            uint, OperationType
        ));

        if (opType == OperationType.Inflate) {
            _supplyDaiInternal(loanAmount);
            _borrowDaiFromCompound(loanAmount);
        } else {
            _repayBorrow(loanAmount);
            _redeemUnderlyingInternal(loanAmount);
        }
    }

    /**
     * @notice Claim COMP tokens
     */
    function claimComp() external onlyOwner {
        comptroller.claimComp(address(this));

        COMP.transfer(owner, COMP.balanceOf(address(this)));
    }
}